#!/usr/bin/python
#
# a wrapper for $EDITOR

import os, re, readline, string, subprocess, sys
from optparse import OptionParser

_null_trans = string.maketrans("", "")
text_characters = "".join(map(chr, range(32, 127)) + list("\n\r\t\b"))

editor = os.getenv('EDITOR')

def buildparser():
    usage = "usage: %prog [-h] [-a] [regex1 regex2 ...]"
    parser = OptionParser(usage=usage)
    parser.add_option("-a", "--all", 
                  action="store_true", dest="all", default=False,
                  help="show hidden files")
    parser.add_option("-b", "--bin",
                  action="store_true", dest="bin", default=False,
                  help="show binary files")
    return parser

def isbin(filename, blocksize=512):
    f = open(filename).read(blocksize)
    if f:
        t = f.translate(_null_trans, text_characters)
        if "\0" in f or len(t)/len(f) > 0.30:
            return True

def getfiles(optall, optbin):
    for fn in os.listdir('.'):
        if (os.path.isfile(fn) and os.access(fn,os.R_OK) and
            (optall or not fn.startswith('.')) and
            (optbin or not isbin(fn))):
            yield fn

def filterfiles(args, optall, optbin):
    files = []
    for fn in getfiles(optall,optbin):
        if all(re.search(r,fn) for r in args):
            files.append(fn)
    return files    

def choose(names):
    if len(names) == 1:
        name = names[0]
    else:
        for n, fn in enumerate(names):
            print "%3d %-20s" % (n + 1, fn)
        name = raw_input("  > ")
    if name == "":
        return
    elif name.isdigit() and int(name) - 1 in range(len(names)):
        return names[int(name) - 1]
    else:
        return name

def main():
    if not editor:
        print "$EDITOR not set"
        return
    parser = buildparser()
    (opts, args) = parser.parse_args()
    try:
        name = choose(filterfiles(args, opts.all, opts.bin))
        if name:
            subprocess.Popen([editor, name]).wait()
    except KeyboardInterrupt:
        print

if __name__ == "__main__": 
    main()
