#!/usr/bin/python
# little man page getter for olpc (which doesn't have man installed)

import sys
from subprocess import *
from urllib import urlopen

url = 'http://man.linuxquestions.org/?query=%s&section=0&type=0'

if len(sys.argv) != 2:
    print "What manual page do you want?"
else:
    text = urlopen(url % (sys.argv[1])).read()
    if text.count("\n"):
        text = Popen(["echo", text], stdout=PIPE,stderr=PIPE)
        Popen(["less"], stdin=text.stdout).wait()
    else:
        print text
